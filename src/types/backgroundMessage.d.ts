import meangings from "./meanings";

interface BackgroundMessage {
    definitionArray: meangings[];
    sender: string;
}

export default BackgroundMessage;
