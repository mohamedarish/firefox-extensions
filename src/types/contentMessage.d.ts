interface contentMessage {
	domElement: DOMRect;
	word: string;
	sender: string;
}

export default contentMessage;
